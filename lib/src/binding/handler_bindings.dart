// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.handler;

import 'package:shelf/shelf.dart' show Handler, Request, Response;
import 'dart:mirrors' show ClosureMirror, reflect;
import '../preconditions.dart' show ensure, isNotNull;
import '../util.dart' show syncFuture, iterableToJson;
import 'dart:async' show Future;
import 'package:path/path.dart' as p show url;
import 'validation.dart';

import 'parameter_bindings.dart' show HandlerParameterBinding,
    JsonBodyTypeParameterBinding, TypePathParameterBinding,
    inferHandlerParameterBindings, CustomObjectFactory;
import 'response_bindings.dart' show JsonResponseBinding,
    ResponseBinding, inferResponseBinding;

import 'package:shelf_exception_response/exception.dart'
  show BadRequestException, HttpException;
import 'dart:io' show HttpStatus;
import 'package:shelf_route/shelf_route.dart';

final _url = p.url;

HandlerBinding createHandlerBinding(Function handler,
    List<HandlerParameterBinding> providedBindings,
    ResponseBinding responseBinding,
    Map<Type, CustomObjectFactory> customObjects,
    bool validateParameters, bool validateReturn) {
  return _inferHandlerBindings(handler, providedBindings,
      responseBinding, customObjects, validateParameters,
      validateReturn);
}

/// a [HandlerAdapter] for shelf_bind.
/// It implements [MergeableHandlerAdpater] such that [customObjects]
/// are merged
class BoundHandlerAdapter implements MergeableHandlerAdpater {
  final Map<Type, CustomObjectFactory> customObjects;
  final bool validateParameters;
  final bool validateReturn;

  /// allows the parameter binding to be tweaked per handler. Note it does not
  /// make sense to specify this on handlerAdapters applied at a router level
  final List<HandlerParameterBinding> parameterBindings;

  /// allows the response binding to be tweaked per handler. Note it does not
  /// make sense to specify this on handlerAdapters applied at a router level
  final ResponseBinding responseBinding;

  BoundHandlerAdapter({ this.customObjects: const {},
      this.validateParameters: false, this.validateReturn: false,
      this.parameterBindings: const [], this.responseBinding }) {
    ensure(customObjects, isNotNull);
    ensure(validateParameters, isNotNull);
    ensure(validateReturn, isNotNull);
    ensure(parameterBindings, isNotNull);
  }

  BoundHandlerAdapter merge(BoundHandlerAdapter other) {
    return new BoundHandlerAdapter(
        validateParameters: validateParameters,
        validateReturn: validateReturn,
        parameterBindings: parameterBindings,
        responseBinding: responseBinding,
        customObjects: {}
          ..addAll(customObjects)
          ..addAll(other.customObjects));
  }

  Handler call(Function handler) => handlerAdapter(handler);

  HandlerAdapter get handlerAdapter =>
    (Function handler) => createHandlerBinding(handler,
        parameterBindings, responseBinding, customObjects,
        validateParameters, validateReturn)
        .handler;
}

class HandlerBinding {
  final List<HandlerParameterBinding> parameterBindings;
  final Function _boundHandler;
  final ResponseBinding responseBinding;

  HandlerBinding(this.parameterBindings, Function handler,
      [ ResponseBinding responseBinding,
        bool validateParameters = true, bool validateReturn = false])
      : this._boundHandler = _wrapHandler(handler,
          validateParameters, validateReturn),
        this.responseBinding = responseBinding != null ?
            responseBinding : new JsonResponseBinding.ok() {
    ensure(parameterBindings, isNotNull);
    ensure(_boundHandler, isNotNull);
    parameterBindings.forEach((pb) => pb.validate());
  }


  Handler get handler => _handleRequest;

  dynamic _handleRequest(Request request) {
    final parameterFutureValues =
        parameterBindings.map((pb) => syncFuture(() =>
            pb.createInstance(request)));

    return  _boundHandler(parameterFutureValues, request, responseBinding);

  }


}





typedef _BoundHandler(Iterable<dynamic> positionalArguments, Request request,
    ResponseBinding responseBinding);

HandlerBinding _inferHandlerBindings(Function handler,
    List<HandlerParameterBinding> providedParameterBindings,
    ResponseBinding providedResponseBinding,
    Map<Type, CustomObjectFactory> customObjects,
    bool validateParameters, bool validateReturn) {
  final ClosureMirror functionMirror = reflect(handler);
  final inferredParameterBindings =
      inferHandlerParameterBindings(functionMirror, providedParameterBindings,
          customObjects);

  final inferredResponseBinding = inferResponseBinding(
      functionMirror, providedResponseBinding);

  return new HandlerBinding(inferredParameterBindings, handler,
      inferredResponseBinding, validateParameters, validateReturn);
}







_BoundHandler _wrapHandler(Function handler, bool validateParameters,
                           bool validateReturn) {
  final ClosureMirror handlerMirror = reflect(handler);
  final functionMirror = handlerMirror.function;
  final params = functionMirror.parameters;
  final numParams = params.length;
  final returnType = functionMirror.returnType;

  return (Iterable<dynamic> positionalArguments, Request request,
      ResponseBinding responseBinding) {

    final futureResponse = syncFuture(() =>
        Future.wait(positionalArguments).then((pa) =>
          constrainedFunctionProxy.invoke(handlerMirror, pa,
            validateParameters: validateParameters,
            validateReturn: validateReturn)));

    return futureResponse.then((response) {
      if (response == null) {
        return new Response.internalServerError(body:
          'function ${functionMirror.simpleName} returned null');
      }
      return responseBinding.createResponse(request, reflect(response));
    })
    .catchError((e) => throw new BadRequestException(
        { 'errors': iterableToJson(e.violations) }),
      test: (e) => e is ParameterConstraintViolationException)
    .catchError((e) => throw new BadRequestException(
        { 'errors': e.toString() }, e.toString()),
      test: (e) => e is ArgumentError)
    .catchError((e) => throw new BadRequestException(
        { 'errors': 'Failed to convert request parameters. Invalid value ${e.message}' }),
      test: (e) => e is FormatException)
    .catchError((e) => throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR,
        "Internal Server Error", { 'errors': iterableToJson(e.violations) }),
      test: (e) => e is ReturnConstraintViolationException);
  };
}

