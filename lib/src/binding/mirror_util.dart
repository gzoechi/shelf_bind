// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library mirror.util;

import 'dart:mirrors' show DeclarationMirror, reflectType;

dynamic getAnnotation(DeclarationMirror dm, Type annotationType) {
  final resourceAnnotations = dm.metadata.where(
      (md) => reflectType(annotationType).isAssignableTo(md.type));
  if (resourceAnnotations.isEmpty) {
    return null;
  }

  return resourceAnnotations.first.reflectee;
}