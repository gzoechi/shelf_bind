// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.param;


import 'package:shelf/shelf.dart' show Request;
import 'package:shelf_path/shelf_path.dart' show getPathParameter,
    getPathParameters;


import 'dart:mirrors' show ClosureMirror, InstanceMirror, MirrorSystem,
    ParameterMirror, TypeMirror, reflect, reflectClass;
import '../converters.dart' show convertTo;
import '../preconditions.dart' show ensure, isNotNull;
import '../annotations.dart' show BodyFormat, RequestBody, ContentType;
import 'dart:async' show Future;


import 'type_bindings.dart' show BindMode, TypeBinding, inferTypeBinding;
import 'dart:convert' show JSON;
import 'mirror_util.dart' show getAnnotation;


final ParameterBindingInferencer _defaultParameterInferencer =
    new ParameterBindingInferencer();

Map _mapValues(Map map, convert(v)) {
  final result = {};
  map.forEach((k, v) => result[k] = convert(v));
  return result;
}

List<HandlerParameterBinding>
    inferHandlerParameterBindings(ClosureMirror functionMirror,
    List<HandlerParameterBinding> providedBindings,
    Map<Type, CustomObjectFactory> customObjects) {

  final bindingInferencer = customObjects == null ? _defaultParameterInferencer
      : new ParameterBindingInferencer(
          additionalBindingFactories:
            _mapValues(customObjects,
              (factory) => new CustomObjectParameterBinding(factory)));

  return bindingInferencer.inferHandlerParameterBindings(
      functionMirror, providedBindings);
}

/**
 * A binding from instances of [Request] to a single parameter of a handler
 * function.
 *
 * Different sorts of bindings are supported such as:
 * * the [request] object itself (see [RequestObjectParameterBinding])
 * * path parameters to simple type (see [SimplePathParameterBinding])
 * * path parameters to class types (see [TypePathParameterBinding])
 * * request body in JSON format to class types (see [JsonBodyTypeParameterBinding])
 */
abstract class HandlerParameterBinding {
  const HandlerParameterBinding();
  dynamic createInstance(Request request);
  void validate();
}

typedef CustomObjectFactory(Request request);

class CustomObjectParameterBinding extends HandlerParameterBinding {
  final CustomObjectFactory factory;

  const CustomObjectParameterBinding(this.factory);

  @override
  void validate() {}

  @override
  createInstance(Request request) => factory(request);
}

Request _noopCustomObjectFactory(request) => request;

class RequestObjectParameterBinding extends CustomObjectParameterBinding {
  RequestObjectParameterBinding() : super(_noopCustomObjectFactory);
}

class SimplePathParameterBinding extends HandlerParameterBinding {
  final String pathParameterName;
  final Type simpleType;

  SimplePathParameterBinding(this.pathParameterName, [this.simpleType =
      String]);

  @override
  dynamic createInstance(Request request) {
    // TODO: simple type conversion
    final value = getPathParameter(request, pathParameterName);
    return value == null || simpleType == String ?
        value :
        convertTo(simpleType, value);
  }

  @override
  void validate() {
    ensure(pathParameterName, isNotNull);
  }
}

abstract class _TypeParameterBinding extends HandlerParameterBinding {
  final TypeBinding typeBinding;

  _TypeParameterBinding(this.typeBinding);

  @override
  void validate() {
    ensure(typeBinding, isNotNull);
    typeBinding.validateBindings();
  }

  @override
  createInstance(Request request) => _createUnvalidated(request);

  _createUnvalidated(Request request);


  _TypeParameterBinding copy(TypeBinding newTypeBinding);
}


class TypePathParameterBinding extends _TypeParameterBinding {
  TypePathParameterBinding(TypeBinding typeBinding)
      : super(typeBinding);


  @override
  dynamic _createUnvalidated(Request request) {
    final instanceMirror = _createInstance(getPathParameters(request),
        typeBinding);
    return instanceMirror.reflectee;
  }

  @override
  _TypeParameterBinding copy(TypeBinding newTypeBinding) =>
      new TypePathParameterBinding(newTypeBinding);
}

class JsonBodyTypeParameterBinding extends _TypeParameterBinding {
  JsonBodyTypeParameterBinding(TypeBinding typeBinding)
    : super(typeBinding);

  @override
  dynamic _createUnvalidated(Request request) {
    return _extractJsonBody(request).then((json) {
      final classMirror = reflectClass(typeBinding.type);
      final instanceMirror = classMirror.newInstance(#fromJson, [json]);

      return instanceMirror.reflectee;
    });
  }

  Future<Map> _extractJsonBody(Request request) =>
      request.readAsString().then((s) => JSON.decode(s));

  @override
  _TypeParameterBinding copy(TypeBinding newTypeBinding) =>
      new JsonBodyTypeParameterBinding(newTypeBinding);

}

class FormBodyTypeParameterBinding extends _TypeParameterBinding {
  FormBodyTypeParameterBinding(TypeBinding typeBinding)
      : super(typeBinding);


  @override
  dynamic _createUnvalidated(Request request) {
    return _extractFormBody(request).then((formParams) {
      final instanceMirror = _createInstance(formParams,
          typeBinding);
      return instanceMirror.reflectee;
    });
  }

  Future<Map> _extractFormBody(Request request) =>
      request.readAsString().then((s) => Uri.splitQueryString(s));

  @override
  _TypeParameterBinding copy(TypeBinding newTypeBinding) =>
      new FormBodyTypeParameterBinding(newTypeBinding);

}



InstanceMirror _createInstance(Map<String, Object> parameters,
                               TypeBinding binding) {
  switch (binding.bindMode) {
    case BindMode.CONSTRUCTOR:
      return _createInstanceViaConstructor(parameters, binding);
    case BindMode.PROPERTY:
      return _createInstanceViaProperty(parameters, binding);

    default:
      throw new StateError('unsupported bind mode: ${binding.bindMode}');
  }
}

InstanceMirror _createInstanceViaConstructor(Map<String, Object> parameters,
    TypeBinding binding) {
  final namedArguments = _bindVariables(parameters, binding.memberBindings);
  final classMirror = reflectClass(binding.type);
  final instanceMirror =
      classMirror.newInstance(binding.constructorName, [], namedArguments);
  return instanceMirror;
}

InstanceMirror _createInstanceViaProperty(Map<String, Object> parameters,
                                          TypeBinding binding) {
  final properties = _bindVariables(parameters, binding.memberBindings);
  final classMirror = reflectClass(binding.type);
  final instanceMirror = classMirror.newInstance(const Symbol(''), []);

  properties.forEach((property, value) {
    instanceMirror.setField(property, value);
  });
  return instanceMirror;
}

Map<Symbol, dynamic> _bindVariables(Map<String, Object> parameters,
    Map<Symbol, Symbol> memberBindings) {

  final convertedPv = <Symbol, Object>{};
  parameters.forEach((k, v) {
    convertedPv[new Symbol(k)] = v;
  });

  final namedArguments = <Symbol, dynamic>{};
  memberBindings.forEach((k, v) {
    final value = convertedPv[k];
    if (value != null) {
      namedArguments[v] = value;
    }
  });

  return namedArguments;
}



class ParameterBindingInferencer {
  static final Map<Type, HandlerParameterBinding> _defaultBingingFactories = {
    Request: new RequestObjectParameterBinding()
  };

  final Map<Type, HandlerParameterBinding> bindingFactories;

  ParameterBindingInferencer({Map<Type, HandlerParameterBinding> additionalBindingFactories})
      : this.bindingFactories = additionalBindingFactories != null ?
          ({}..addAll(additionalBindingFactories)
          ..addAll(_defaultBingingFactories)) : _defaultBingingFactories;

  List<HandlerParameterBinding>
      inferHandlerParameterBindings(ClosureMirror functionMirror,
      List<HandlerParameterBinding> providedBindings) {
    final params = functionMirror.function.parameters;
    final inferredBindings = <HandlerParameterBinding>[];

    for (int i = 0; i < params.length; i++) {
      final param = params[i];
      final providedBinding = i > providedBindings.length - 1 ?
          null :
          providedBindings.elementAt(i);
      final parameterBinding =
          _inferHandlerParameterBinding(providedBinding, param);
      if (parameterBinding != null) {
        inferredBindings.add(parameterBinding);
      }
    }
    return inferredBindings;
  }



  HandlerParameterBinding
      _inferHandlerParameterBinding(HandlerParameterBinding providedBinding,
      ParameterMirror param) {
    // TODO: better name than defaultBinding
    final defaultBinding = bindingFactories[param.type.reflectedType];
    if (defaultBinding != null) {
      return defaultBinding;
    }

    if (_isSimpleType(param.type)) {
      return _inferSimplePathParameterBinding(providedBinding, param);
    } else {
      return _inferTypeParameterBinding(providedBinding, param);
    }
  }

  HandlerParameterBinding
      _inferSimplePathParameterBinding(HandlerParameterBinding providedBinding,
      ParameterMirror param) {

    if (providedBinding != null && providedBinding is _TypeParameterBinding) {
      throw new ArgumentError(
          'type bindings not supported for dart core classes');
    }

    final functionParameterName = MirrorSystem.getName(param.simpleName);
    final providedName = providedBinding == null ?
        null :
        (providedBinding as SimplePathParameterBinding).pathParameterName;

    final pathParameterName = providedName != null ?
        providedName :
        functionParameterName;

    return new SimplePathParameterBinding(
        pathParameterName,
        param.type.reflectedType);

  }

  HandlerParameterBinding
      _inferTypeParameterBinding(HandlerParameterBinding providedBinding,
      ParameterMirror param) {

    if (providedBinding != null &&
        providedBinding is SimplePathParameterBinding) {
      throw new ArgumentError(
          'simple bindings not supported for non dart core classes');
    }

    providedBinding = _inferTypeParameterBindingFromAnnotation(
        providedBinding, param);

    final providedTypeBinding = providedBinding == null ?
        new TypeBinding(null) :
        (providedBinding as _TypeParameterBinding).typeBinding;

    final inferredTypeBinding = inferTypeBinding(providedTypeBinding, param);

    final inferredParameterBinding = providedBinding == null ?
        new TypePathParameterBinding(inferredTypeBinding) :
          (providedBinding as _TypeParameterBinding).copy(inferredTypeBinding);

    inferredParameterBinding.validate();
    return inferredParameterBinding;
  }


  HandlerParameterBinding
      _inferTypeParameterBindingFromAnnotation(
      HandlerParameterBinding providedBinding,
      ParameterMirror param) {

    final RequestBody requestBodyAnnotation = getAnnotation(param, RequestBody);

    if (requestBodyAnnotation == null) {
      return providedBinding;
    }

    final TypeBinding typeBinding = new TypeBinding(requestBodyAnnotation.type);

    switch(requestBodyAnnotation.format) {
      case ContentType.JSON:
        return new JsonBodyTypeParameterBinding(typeBinding);
      case ContentType.FORM:
        return new FormBodyTypeParameterBinding(typeBinding);
      default:
        throw new ArgumentError("Only JSON and FORM bodies currently supported");
    }
  }

  List<TypeBinding> _inferMissingBindings(Function handler,
      List<TypeBinding> typeBindings) {

    final ClosureMirror functionMirror = reflect(handler);
    final params = functionMirror.function.parameters;
    final inferredBindings = <TypeBinding>[];

    for (int i = 0; i < params.length; i++) {
      final param = params[i];
      final providedBinding = i > typeBindings.length - 1 ?
          new TypeBinding(null) :
          typeBindings.elementAt(i);
      final typeBinding = inferTypeBinding(providedBinding, param);
      if (typeBinding != null) {
        inferredBindings.add(typeBinding);
      }
    }

    return inferredBindings;
  }

// TODO: Not a good way to determine this
  bool _isSimpleType(TypeMirror tm) =>
      (tm.isTopLevel && tm.owner.simpleName == #dart.core);
}
