// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.bindings.core;

import 'package:shelf/shelf.dart' show Request;
import 'dart:mirrors' show ClassMirror, MethodMirror, MirrorSystem, 
    ParameterMirror, TypeMirror, VariableMirror, reflectType;
import '../preconditions.dart' show ensure, isEmpty, isNot, isNotNull;
import 'package:quiver/collection.dart' show BiMap;




class BindMode {
  final String literal;

  const BindMode._internal(this.literal);

  static const BindMode CONSTRUCTOR = const BindMode._internal("CONSTRUCTOR");
  static const BindMode PROPERTY = const BindMode._internal("PROPERTY");
  static const BindMode CONSTRUCTOR_THEN_PROPERTY =
      const BindMode._internal("CONSTRUCTOR_THEN_PROPERTY");
}


class TypeBinding {
  final Type type;
  final Symbol constructorName;
  final Map<Symbol, Symbol> memberBindings;
  final BindMode bindMode;

  TypeBinding._internal(this.type, this.constructorName, this.bindMode,
      this.memberBindings) {
//    _validateBindings(); TODO: don't like turning this off
  }

  TypeBinding(Type type, {Symbol constructorName: #build, 
    BindMode bindMode: BindMode.CONSTRUCTOR_THEN_PROPERTY, 
      Map<String, Symbol> memberBindings: const  {}})
      : this._internal(
          type,
          constructorName,
          bindMode,
          _convertBindings(memberBindings));

  static Map<Symbol, Symbol> _convertBindings(Map<String,
      Symbol> memberBindings) {

    if (memberBindings == null) {
      return const {};
    }

    final convertedBindings = <Symbol, Symbol>{};
    memberBindings.forEach((n, s) {
      convertedBindings[new Symbol(n)] = s;
    });
    return convertedBindings;
  }

  validateBindings() {
    ensure(type, isNotNull);
    ensure(constructorName, isNotNull);
    ensure(memberBindings, isNotNull);
    ensure(memberBindings, isNot(isEmpty));
    ensure(bindMode, isNotNull);
  }
}


TypeBinding inferTypeBinding(TypeBinding providedBinding, ParameterMirror pm) {
  final parameterType = pm.type;

  final constructorName = providedBinding.constructorName;
  final bindMode = providedBinding.bindMode;

  final typeMirror = _inferType(providedBinding.type, parameterType);
  if (!(typeMirror is ClassMirror)) {
    return null;
  }

  final cm = typeMirror;

  if (cm.isTopLevel && cm.owner.simpleName == #dart.core) {
    return null;
  }

  if (cm.reflectedType == Request) {
    return null;
  }

  final type = typeMirror.reflectedType;

  final bindModeAndBindings =
      _inferMemberBindings(
          providedBinding.memberBindings,
          typeMirror,
          bindMode,
          constructorName);

  return new TypeBinding._internal(
      type,
      constructorName,
      bindModeAndBindings.bindMode,
      bindModeAndBindings.memberBindings);
}

TypeMirror _inferType(Type providedType, TypeMirror parameterType) {
  return providedType != null ? reflectType(providedType) : parameterType;
}

class _ModeBindingPair {
  final BindMode bindMode;
  final Map<Symbol, Symbol> memberBindings;

  _ModeBindingPair(this.bindMode, this.memberBindings);
}

_ModeBindingPair _inferMemberBindings(Map<Symbol, Symbol> providedBindings,
    ClassMirror parameterType, BindMode bindMode, Symbol constructorName) {
  switch (bindMode) {
    case BindMode.CONSTRUCTOR_THEN_PROPERTY:
      return _inferBindingsViaConstructorThenProperty(
          providedBindings,
          parameterType,
          constructorName);
    case BindMode.CONSTRUCTOR:
      return new _ModeBindingPair(
          BindMode.CONSTRUCTOR,
          _inferMemberBindingsViaConstructor(
              providedBindings,
              parameterType,
              constructorName));
    case BindMode.PROPERTY:
      return new _ModeBindingPair(
          BindMode.PROPERTY,
          _inferMemberBindingsViaProperty(providedBindings, parameterType));

    default:
      throw new StateError('unsupported bind mode: ${bindMode}');
  }
}

_ModeBindingPair _inferBindingsViaConstructorThenProperty(Map<Symbol,
    Symbol> providedBindings, ClassMirror parameterType, Symbol constructorName) {

  final viaCtr =
      _inferMemberBindingsViaConstructor(
          providedBindings,
          parameterType,
          constructorName,
          throwOnMissingCtr: false);

  final bindMode = viaCtr != null ? BindMode.CONSTRUCTOR : BindMode.PROPERTY;

  final inferredBindings = viaCtr != null ?
      viaCtr :
      _inferMemberBindingsViaProperty(providedBindings, parameterType);

  return new _ModeBindingPair(bindMode, inferredBindings);
}


Map<Symbol, Symbol> _inferMemberBindingsViaConstructor(Map<Symbol,
    Symbol> providedBindings, ClassMirror parameterType, Symbol constructorName,
    {throwOnMissingCtr: true}) {
  final _providedBindings = new BiMap()..addAll(providedBindings);

  final fullCtrName =
      new Symbol(
          "${MirrorSystem.getName(parameterType.simpleName)}"
              ".${MirrorSystem.getName(constructorName)}");

  final MethodMirror ctrMirror = parameterType.declarations[fullCtrName];
  if (ctrMirror == null) {
    if (throwOnMissingCtr) {
      throw new ArgumentError('No such constructor $fullCtrName');
    } else {
      return null;
    }
  }

  if (!ctrMirror.isConstructor) {
    throw new ArgumentError('$fullCtrName is not a constructor');
  }

  final memberBindings = <Symbol, Symbol>{};

  ctrMirror.parameters.forEach((pm) {
    final parameterName = pm.simpleName;
    final providedName = _providedBindings.inverse[parameterName];

    final requestName = providedName != null ? providedName : parameterName;
    memberBindings[requestName] = parameterName;
  });

  return memberBindings;
}

Map<Symbol, Symbol> _inferMemberBindingsViaProperty(Map<Symbol,
    Symbol> providedBindings, ClassMirror parameterType) {

  final _providedBindings = new BiMap()..addAll(providedBindings);

  final memberBindings = <Symbol, Symbol>{};

  parameterType.declarations.forEach((s, dm) {
    if (dm is VariableMirror || (dm is MethodMirror && dm.isGetter)) {
      final propertyName = dm.simpleName;
      final providedName = _providedBindings.inverse[propertyName];

      final requestName = providedName != null ? providedName : propertyName;
      memberBindings[requestName] = propertyName;
    }
  });

  return memberBindings;
}


TypeBinding _createTypeMapping(Type type, Symbol constructorName, Map<String,
    Symbol> memberBindings, BindMode bindMode) {
  return new TypeBinding(
      type,
      constructorName: constructorName,
      bindMode: bindMode,
      memberBindings: memberBindings);
}
