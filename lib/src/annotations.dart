// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.annotation;
import 'dart:io' show HttpStatus;

/**
 * An annotation that indicates that a parameter should be bound to the request
 * body. The body will be parsed into an object of the specified [type].
 *
 * The default format for the body is JSON. In the future this will be
 * able to be overriden by specifying a value for [format]
 */
class RequestBody {
  final ContentType format;
  final Type type;

  const RequestBody({ this.format: ContentType.JSON, this.type });
}

///**
// * An enum specifying the format for a [Request] body
// */
//class BodyFormat {
//  final String name;
//  final ContentType contentType;
//
//  const BodyFormat._internal(this.name);
//
//  static const BodyFormat JSON = const BodyFormat._internal("JSON");
//}


/**
 * Annotation to tweak default creating of responses
 */
class ResponseHeaders {
  /// The HttpStatus to use if the handler returns normally (no exception thrown).
  /// Default is OK. Most common use would be to return CREATED instead for
  /// certain operations
  final int successStatus;

  /// Set to [true] if the location header should be populated automatically.
  /// Default is false.
  /// If [true] the location header is populated to the incoming request url,
  /// which is normally correct if creating via a PUT (but not POST)
  final bool setLocation;
  
  /// The name of the field that is the primary key for the class. 
  /// This will be used when setting the location header.
  /// Default is `id`
  final Symbol idField;


  const ResponseHeaders({ this.successStatus: HttpStatus.OK,
    this.setLocation: false, this.idField: #id });

  /// Return a CREATED status code and populate the location automatically
  const ResponseHeaders.created({ Symbol idField: #id }) :
    this(successStatus: HttpStatus.CREATED, setLocation: true, idField: idField);
}

//class ResponseX {
//  final RequestBody body;
//  final ResponseHeaders headers;
//
//  const ResponseX({ this.body: const RequestBody(),
//    this.headers: ResponseHeaders.OK });
//}




/**
 * Representation of a content type. An instance of [ContentType] is
 * immutable.
 */
class ContentType  {
  /**
   * Gets the mime-type, without any parameters.
   */
  String get mimeType =>'$primaryType/$subType';

  /**
   * Gets the primary type.
   */
  final String primaryType;
  
  /**
   * Gets the sub type.
   */
  final String subType;
  
  /**
   * Gets the character set.
   */
  final String charset;
  

  /**
   * Content type for plain text using UTF-8 encoding.
   *
   *     text/plain; charset=utf-8
   */
  static const TEXT = const ContentType._internal("text", "plain", charset: "utf-8");

  /**
   *  Content type for HTML using UTF-8 encoding.
   *
   *     text/html; charset=utf-8
   */
  static const HTML = const ContentType._internal("text", "html", charset: "utf-8");

  /**
   *  Content type for JSON using UTF-8 encoding.
   *
   *     application/json; charset=utf-8
   */
  static const JSON = const ContentType._internal("application", "json", charset: "utf-8");

  /**
   *  Content type for binary data.
   *
   *     application/octet-stream
   */
  static const BINARY = const ContentType._internal("application", "octet-stream");

  /**
   *  Content type for form data using UTF-8 encoding.
   *
   *     application/x-www-form-urlencoded; charset=utf-8
   */
  static const FORM = const ContentType._internal("application", 
      "x-www-form-urlencoded", charset: "utf-8");

  /**
   *  Content type for multipart form data.
   *
   *     multipart/form-data
   */
  static const MULTIPART = const ContentType._internal("multipart", 
      "form-data");
  

  const ContentType._internal(this.primaryType,
                      this.subType,
                      {this.charset});

}
