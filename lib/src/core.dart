// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.core;

import 'package:shelf/shelf.dart';
import 'package:shelf_route/shelf_route.dart';
import 'bindings.dart';
export 'bindings.dart';

/**
 * Specify [customObjects] to suport injection of arbitraty objects into
 * your handler functions. When the binders encounter such parameters in your
 * function it will call the factory you provide in [customObjects] instead of
 * performing the normal binding process.
 *
 * This is typically used for injecting objects that aren't simply a
 * representation of the data that is contained in the request, but rather some
 * auxiliary object, such as an authenticated client to some service. The
 * construction of that client typically depends on properties of the request
 * to establish authentication.
 *
 * ```
 * // a factory for your client
 * FooClient createFooClient(Request request) => ... create client somehow
 *
 * // pass it as a factory for your client using [customObjects] parameter
 * to [handlerAdapter]
 * var myHandlerAdapter = handlerAdapter(customObjects:
 *    { FooClient: createFooClient });
 *
 * var myRouter = router(handlerAdpater: myHandlerAdapter);
 *
 * myRouter.get('/person/{name}', (FooClient fooClient, String name) => ...);
 * ```
 *
 * [validateParameters] and [validateReturn] have the same meanings as for
 * the [bind] function. They will apply to all methods bound via this adapter
 */
HandlerAdapter handlerAdapter({
    Map<Type, CustomObjectFactory> customObjects: const {},
    bool validateParameters: false, bool validateReturn: false }) {
  return new BoundHandlerAdapter(customObjects: customObjects,
      validateParameters: validateParameters,
      validateReturn: validateReturn);
}

/**
 * Bind properties (path parameters, body) of a [Request] to the parameters
 * of a [handler] function.
 *
 * Optionally the bindings can be specified by passing [parameterBindings].
 * The order of the [parameterBindings] must match the order of the parameters
 * to the function.
 *
 * Additionally, bindings can be configured via annotations. This is typically
 * more convenient than passing [parameterBindings] to the bind method.
 *
 * When the [parameterBindings] and annotations are omitted, the bindings are
 * automatically inferred.
 *
 * The inferrence supports all the features of the [HandlerParameterBinding]s
 * including:
 * * binding to request path parameters
 * * binding to json body
 * * binding via constructor
 * * binding via setters
 * * etc
 *
 * Consult the wiki for details on the inference process
 *
 * If [validateParameters] is set to true then the [constrain] package will be
 * used to validate all handler parameters (except simple parameters).
 * If validation fails then a [BadRequestException] will be thrown with the
 * constraint violations included in the error. This is useful as preconditions
 * to your handlers so that your handler code knows it's dealing with valid
 * data.
 *
 * If [validateReturn] is set to true then the [constrain] package will be
 * used to validate the result object before it is returned. Similar handling
 * as per [validationParamters] occurs on validation failure. This is useful
 * as a post condition on your handlers to make sure you don't return invalid
 * objects.
 */
Handler bind(Function handler,
      { List<HandlerParameterBinding> parameterBindings: const [],
        ResponseBinding responseBinding,
        Map<Type, CustomObjectFactory> customObjects,
        bool validateParameters: false, bool validateReturn: false }) {
  return createHandlerBinding(handler, parameterBindings,
      responseBinding, customObjects, validateParameters, validateReturn)
      .handler;
}


