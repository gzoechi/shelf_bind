// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.convert;

import 'dart:convert';

// TODO: this should be moved to a standalone package on pub

convertTo(Type type, String value) =>
_codec(type).decode(value);

String convertFrom(Type type, dynamic value) =>
_codec(type).encode(value);

typedef _To(String s);

typedef String _From(v);

class SympleTypeCodec<T> extends Codec<T, String> {
  @override
  final Converter<String, T> decoder;

  @override
  final Converter<T, String> encoder;

  SympleTypeCodec(T to(String s), String from(T t))
  : this.decoder = new _ToConverter(to),
  this.encoder = new _FromConverter(from);
}

class _ToConverter<T> extends Converter<String, T> {
  final _To to;

  const _ToConverter(T to(String s))
  : this.to = to;

  @override
  T convert(String input) => to(input);
}

class _FromConverter<T> extends Converter<T, String> {
  final _From from;

  const _FromConverter(String from(T t))
      : this.from = from;

  @override
  String convert(T input) => from(input);
}

final SympleTypeCodec<num> numCodec = new SympleTypeCodec(
        (s) => num.parse(s), (n) => n.toString());

final SympleTypeCodec<bool> boolCodec = new SympleTypeCodec(
        (s) => s == "true" ? true : false, (b) => b.toString());

final SympleTypeCodec<DateTime> dateTimeCodec = new SympleTypeCodec(
        (s) => DateTime.parse(s), (d) => d.toString());

final SympleTypeCodec<Uri> uriCodec = new SympleTypeCodec(
        (s) => Uri.parse(s), (u) => u.toString());


final Map<Type, Codec<dynamic, String>> _stringCodecs = {
    num: numCodec,
    int: numCodec, // TODO: avoid by checking 'is'
    double: numCodec,
    bool: boolCodec,
    DateTime: dateTimeCodec,
    Uri: uriCodec
};

_codec(Type type) {
  final codec = _stringCodecs[type];
  if (codec == null) {
    throw new ArgumentError('no codec for type $type');
  }
  return codec;
}