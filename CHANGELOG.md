## 0.7.0+3

* increased upper bound on constrain dependency

## 0.7.0+2

* bumped self_exception_response dependency version

## 0.7.0+1

* increased upper bound on shelf_route dependency

## 0.7.0

* added support for mergeable handler adapters. This allows things like
customObjects to be extended by child routes

* removed deprecated methods

## 0.6.1+4

* widened shelf_route version range

## 0.6.1+3

* widened shelf_route version range

## 0.6.1+2

* minor doco changes

## 0.6.1+1

* Improved handling of some errors

## 0.6.1

* Improved integration with [constrain](https://pub.dartlang.org/packages/constrain) package
    * Support for constraints directly on parameters and returns
    * Constraint violations for all parameters included

## 0.6.0+1

* Fixed use of Deprecated

## 0.6.0

* New verson of shelf_route that fixed spelling mistake in HandlerAdapter

## 0.5.1

* Support for form encoded body

## 0.5.0

* **breaking change**: handlerAdapter is now a function
* Support for Custom parameter injection
* Integration with the [constrain](https://pub.dartlang.org/packages/constrain) package for automatic validation of
    * handler function parameters
    * handler function returns

## 0.4.2

* Added idField to ResponseHeaders annotation

## 0.4.1

* Added type converters for bool and DateTime

## 0.4.0
Major release with lots of changes including:

* Extensive binding support:
    * binding multiple function parameters,
    * simple type parameters,
    * custom classes (both to path parameters and body)
* Seamless integration with Shelf Route
* New annotations to customise bindings
* Simple type conversions
* Improved documentation
* Support for Shelf Path to remove direct dependency to Shelf Route

## 0.3.0

* Upgraded shelf and shelf route

## 0.2.2

* Made second argument (the request) to handler optional.

* Support for returning an object and automatically transforming to json

## 0.2.1

* Added support for binding to properties instead of to constructor args

## 0.2.0

* Upgrade shelf to 0.5.0 and shelf route to 0.4.0

## 0.1.0

* First version
