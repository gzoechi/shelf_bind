// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.handler.adapter.test;

import 'package:shelf_bind/src/core.dart';
import 'package:shelf/shelf.dart';
import 'package:unittest/unittest.dart';

main() {
  BoundHandlerAdapter ha1;
  BoundHandlerAdapter ha2;
  Foo foo = new Foo();
  Bar bar = new Bar();
  Function fooAdapter = (_) => foo;
  Function barAdapter = (_) => bar;

  setUp(() {
    ha1 = new BoundHandlerAdapter(customObjects: {
      Foo: fooAdapter
    }, validateParameters: true, validateReturn: true);

    ha2 = new BoundHandlerAdapter(customObjects: {
      Bar: barAdapter
    }, validateParameters: false, validateReturn: false);
  });

  group('ha1.merge(ha2)', () {
    BoundHandlerAdapter ha3;
    Handler boundHandler;
    String response;

    setUp(() {
      ha3 = ha1.merge(ha2);
      boundHandler = ha3(fooHandler);
      return boundHandler(null).then((Response r) => r.readAsString()).
          then((resp) {
        response = resp;
      });
    });

    test('contains both custom objects', () {
      expect(ha3.customObjects.values,
          unorderedEquals([fooAdapter, barAdapter]));
    });

    test('validateReturn taken from first object', () {
      expect(ha3.validateReturn, equals(ha1.validateReturn));
    });

    test('validateParameters taken from first object', () {
      expect(ha3.validateParameters, equals(ha1.validateParameters));
    });

    test('boundHandler works as expected', () {
      expect(response, equals('"FooBar"'));
    });
  });

  group('ha2.merge(ha1)', () {
    BoundHandlerAdapter ha3;
    Handler boundHandler;
    String response;

    setUp(() {
      ha3 = ha2.merge(ha1);
      boundHandler = ha3(fooHandler);
      return boundHandler(null).then((Response r) => r.readAsString()).
          then((resp) {
        response = resp;
      });
    });

    test('contains both custom objects', () {
      expect(ha3.customObjects.values,
          unorderedEquals([fooAdapter, barAdapter]));
    });

    test('validateReturn taken from first object', () {
      expect(ha3.validateReturn, equals(ha2.validateReturn));
    });

    test('validateParameters taken from first object', () {
      expect(ha3.validateParameters, equals(ha2.validateParameters));
    });

    test('boundHandler works as expected', () {
      expect(response, equals('"FooBar"'));
    });
  });
}

class Foo {
}

class Bar {
}

String fooHandler(Foo foo, Bar bar) {
  return '${foo.runtimeType}${bar.runtimeType}';
}