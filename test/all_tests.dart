// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.
library constrain.all.test;

import 'package:unittest/unittest.dart';
import 'shelf_bind_test.dart' as bind;
import 'converters_test.dart' as converters;
import 'handler_adapter_test.dart' as handler_adapter;

main() {
  group('[bind]', bind.main);
  group('[handler_adapter]', handler_adapter.main);
  group('[converters]', converters.main);
}

