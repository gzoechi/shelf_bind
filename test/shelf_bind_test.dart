// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf.bind.test;

import 'package:shelf_bind/shelf_bind.dart' as sb;
import 'package:shelf/shelf.dart' as s;
import 'package:shelf_route/shelf_route.dart' as sr;
import 'package:shelf_path/shelf_path.dart' as sp;
import 'package:constrain/constrain.dart';

import 'package:unittest/unittest.dart';
import 'package:mock/mock.dart';
import 'dart:convert';
import 'dart:async';
import 'package:shelf_bind/src/util.dart';
import 'package:shelf_exception_response/exception.dart';

typedef _PersonHandler1(AbstractPerson person);
typedef _PersonHandler2(AbstractPerson person, s.Request request);

typedef _ConstructorPersonHandler1(ConstructorPerson person);
typedef _ConstructorPersonHandler2(ConstructorPerson person, s.Request request);

typedef _FieldPersonHandler1(FieldPerson person);
typedef _FieldPersonHandler2(FieldPerson person, s.Request request);

typedef _SetterPersonHandler1(SetterPerson person);
typedef _SetterPersonHandler2(SetterPerson person, s.Request request);

typedef _SimpleParameterHandler(String name);
typedef _SimpleNumParameterHandler(int id);

abstract class PersonHandler1Class {
  call(AbstractPerson person) { return null; }
}

abstract class PersonHandler2Class {
  call(AbstractPerson person, s.Request request) { return null; }
}

abstract class SimpleParameterHandlerClass {
  call(String name) { return null; }
}

abstract class SimpleNumParameterHandlerClass {
  call(int id) { return null; }
}

class MockPersonHandler1 extends Mock implements PersonHandler1Class {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}

class MockPersonHandler2 extends Mock implements PersonHandler2Class {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}

class MockSimpleParameterHandler extends Mock implements
    SimpleParameterHandlerClass {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}

class MockSimpleNumParameterHandler extends Mock implements
  SimpleNumParameterHandlerClass {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}

MockPersonHandler1 mockHandler1;

@sb.ResponseHeaders.created(idField: #name)
tweakedResponseHandler(@sb.RequestBody() ConstructorPerson person) =>
  mockHandler1.call(person);

main() {

  MockPersonHandler2 mockHandler2;
  MockSimpleParameterHandler singleStringArgHandler;
  MockSimpleNumParameterHandler singleNumArgHandler;



  setUp(() {
    mockHandler1 = new MockPersonHandler1();
    mockHandler2 = new MockPersonHandler2();
    singleStringArgHandler = new MockSimpleParameterHandler();
    singleNumArgHandler = new MockSimpleNumParameterHandler();
  });

  group('bind', () {
    void runTests(Type personType, sb.BindMode bindMode, singleArgMockHandler,
                  twoArgMockHandler,
                  { Map<String, Symbol> bindings: const {'name': #name} }) {
      s.Handler _bindHandlerFor(wrappedHandler) =>
          sb.bind(wrappedHandler,
              parameterBindings:
                [
                 new sb.TypePathParameterBinding(
                     new sb.TypeBinding(personType, bindMode: bindMode,
                         memberBindings: bindings))
                ]);

      s.Handler _singleArgBindHandler() =>
          _bindHandlerFor(singleArgMockHandler);
      s.Handler _twoArgBindHandler() => _bindHandlerFor(twoArgMockHandler);


      test('should bind to variable in uri path', () {
        mockHandler2.when(callsTo('call')).alwaysReturn(new s.Response.ok(''));
        final Uri uri = Uri.parse('http://localhost/person/fred');
        final bindHandler = _twoArgBindHandler();
        final routeHandler = (sr.router()..get('/person/{name}', bindHandler))
            .handler;

        final request = new s.Request('GET', uri);
        final responseFuture = syncFuture(() => routeHandler(request));
        responseFuture.then((_) {
          mockHandler2.calls('call', personWithName(equals('fred')))
              .verify(happenedOnce);
        });
        expect(responseFuture, completes);
      });

      test('should bind to variable in query parameter', () {
        mockHandler2.when(callsTo('call')).alwaysReturn(new s.Response.ok(''));
        final Uri uri = Uri.parse('http://localhost/person?name=fred');
        final bindHandler = _twoArgBindHandler();
        final routeHandler = (sr.router()..get('/person{?name}', bindHandler))
            .handler;

        final request = new s.Request('GET', uri);
        final responseFuture = syncFuture(() => routeHandler(request));
        responseFuture.then((_) {
          mockHandler2.calls('call', personWithName(equals('fred')))
              .verify(happenedOnce);
        });
        expect(responseFuture, completes);
      });

      test('should support functions with only one argument', () {
        mockHandler1.when(callsTo('call')).alwaysReturn(new s.Response.ok(''));

        final Uri uri = Uri.parse('http://localhost/person/fred');
        final bindHandler = _singleArgBindHandler();
        final routeHandler = (sr.router()..get('/person/{name}', bindHandler))
            .handler;

        final request = new s.Request('GET', uri);
        final responseFuture = syncFuture(() => routeHandler(request));
        responseFuture.then((_) {
          mockHandler1.calls('call', personWithName(equals('fred')))
              .verify(happenedOnce);
        });
        expect(responseFuture, completes);
      });

      test('should convert non Response values to json', () {
        mockHandler1.when(callsTo('call'))
            .alwaysReturn(new ConstructorPerson('barney'));
        final Uri uri = Uri.parse('http://localhost/person/fred');
        final bindHandler = _singleArgBindHandler();
        final routeHandler = (sr.router()..get('/person/{name}', bindHandler))
            .handler;

        final request = new s.Request('GET', uri);
        final responseFuture = routeHandler(request);
        responseFuture.then((r) {
          expect(r, new isInstanceOf<s.Response>());
          s.Response response = r;
          expect(response.headers['content-type'], equals('application/json'));
          response.readAsString().then((str) {
            expect(JSON.decode(str)['name'], equals('barney'));
          });
        });
        expect(responseFuture, completes);
      });
    }

    void runForMode(Type personType, sb.BindMode bindMode,
                    singleArgMockHandler, twoArgMockHandler) {

      group('', () {
        runTests(personType, bindMode,
            singleArgMockHandler, twoArgMockHandler);
      });

      group('with no type provided', () {
        runTests(null, bindMode,
            singleArgMockHandler, twoArgMockHandler);
      });

      group('with no type or member binding provided', () {
        runTests(null, bindMode,
            singleArgMockHandler, twoArgMockHandler, bindings: null);
      });
    }

    group('in constructor mode', () {
      _ConstructorPersonHandler1 singleArgMockHandler() =>
          (ConstructorPerson person) => mockHandler1.call(person);

      _ConstructorPersonHandler2 twoArgMockHandler() =>
          (ConstructorPerson person, s.Request request) =>
              mockHandler2.call(person, request);

      runForMode(ConstructorPerson, sb.BindMode.CONSTRUCTOR,
                 singleArgMockHandler(), twoArgMockHandler());
    });

    group('in property mode with variable', () {
      _FieldPersonHandler1 singleArgMockHandler() =>
          (FieldPerson person) => mockHandler1.call(person);

      _FieldPersonHandler2 twoArgMockHandler() =>
          (FieldPerson person, s.Request request) =>
              mockHandler2.call(person, request);

      runForMode(FieldPerson, sb.BindMode.PROPERTY,
                 singleArgMockHandler(), twoArgMockHandler());

    });

    group('in property mode with setter', () {
      _SetterPersonHandler1 singleArgMockHandler() =>
          (SetterPerson person) => mockHandler1.call(person);

      _SetterPersonHandler2 twoArgMockHandler() =>
          (SetterPerson person, s.Request request) =>
              mockHandler2.call(person, request);

      runForMode(SetterPerson, sb.BindMode.PROPERTY,
                 singleArgMockHandler(), twoArgMockHandler());

    });

    group('should infer simple parameter bindings', () {
      _SimpleParameterHandler singleArgMockHandler() =>
          (String name) => singleStringArgHandler.call(name);

      _SimpleNumParameterHandler singleNumArgMockHandler() =>
          (int id) => singleNumArgHandler.call(id);

      setUp(() {
        singleStringArgHandler.when(callsTo('call'))
            .alwaysReturn(new ConstructorPerson('barney'));
        singleNumArgHandler.when(callsTo('call'))
            .alwaysReturn(new ConstructorPerson('barney'));
      });

      test('', () {
        final Uri uri = Uri.parse('http://localhost/person/fred');
        final bindHandler = sb.bind(singleArgMockHandler());
        final routeHandler = (sr.router()..get('/person/{name}', bindHandler))
            .handler;

        final request = new s.Request('GET', uri);
        final responseFuture = routeHandler(request);

        responseFuture.then((r) {
          singleStringArgHandler.calls('call').verify(happenedOnce);
          singleStringArgHandler.calls('call', 'fred').verify(happenedOnce);
          expect(r, new isInstanceOf<s.Response>());
          s.Response response = r;
          expect(response.headers['content-type'], equals('application/json'));
          response.readAsString().then((str) {
            expect(JSON.decode(str)['name'], equals('barney'));
          });
        });
        expect(responseFuture, completes);

      });

      test('and convert type', () {
        final Uri uri = Uri.parse('http://localhost/person/10');
        final bindHandler = sb.bind(singleNumArgMockHandler());
        final routeHandler = (sr.router()..get('/person/{id}', bindHandler))
            .handler;

        final request = new s.Request('GET', uri);
        final responseFuture = routeHandler(request);

        responseFuture.then((r) {
          singleNumArgHandler.calls('call').verify(happenedOnce);
          singleNumArgHandler.calls('call', 10).verify(happenedOnce);
          expect(r, new isInstanceOf<s.Response>());
          s.Response response = r;
          expect(response.headers['content-type'], equals('application/json'));
          response.readAsString().then((str) {
            expect(JSON.decode(str)['name'], equals('barney'));
          });
        });
        expect(responseFuture, completes);
      });
    });

    group('should bind to body when annotation present', () {
      _ConstructorPersonHandler1 singleArgMockHandler() =>
          (@sb.RequestBody() ConstructorPerson person) =>
              mockHandler1.call(person);

      _ConstructorPersonHandler2 twoArgMockHandler() =>
          (@sb.RequestBody() ConstructorPerson person, s.Request request) =>
              mockHandler2.call(person, request);

      s.Request _createRequest(Uri uri) {
        final json = JSON.encode(new ConstructorPerson('fred').toJson());
        final body = new Future.sync(() => json.codeUnits).asStream();
        return new s.Request('POST', uri, body: body);
      }

      test('and function has one argument', () {
        mockHandler1.when(callsTo('call'))
            .alwaysReturn(new ConstructorPerson('barney'));
        final Uri uri = Uri.parse('http://localhost/person');
        final bindHandler = sb.bind(singleArgMockHandler());
        final routeHandler = (sr.router()..post('/person', bindHandler))
            .handler;

        final request = _createRequest(uri);
        final responseFuture = syncFuture(() => routeHandler(request));
        responseFuture.then((_) {
          mockHandler1.calls('call', personWithName(equals('fred')))
              .verify(happenedOnce);
        });
        expect(responseFuture, completes);
      });

      test('and function has two arguments', () {
        mockHandler2.when(callsTo('call')).alwaysReturn(new s.Response.ok(''));
        final Uri uri = Uri.parse('http://localhost/person');
        final bindHandler = sb.bind(twoArgMockHandler());
        final routeHandler = (sr.router()..post('/person', bindHandler))
            .handler;

        var request = _createRequest(uri);
        final responseFuture = new Future.sync(() => routeHandler(request))
        .then((_) {
          mockHandler2.calls('call', personWithName(equals('fred')))
              .verify(happenedOnce);
        });
        expect(responseFuture, completes);

      });
    });

    group('should bind to form encoded body when annotation present', () {
      _ConstructorPersonHandler1 singleArgMockHandler() =>
          (@sb.RequestBody(format: sb.ContentType.FORM) ConstructorPerson person) =>
              mockHandler1.call(person);

      s.Request _createRequest(Uri uri) {
        final urlEncodedForm = new Uri.http('x', '/', {'name': 'fred'}).query;
        final body = new Future.sync(() => urlEncodedForm.codeUnits).asStream();
        return new s.Request('POST', uri, body: body);
      }

      test('and function has one argument', () {
        mockHandler1.when(callsTo('call'))
            .alwaysReturn(new ConstructorPerson('barney'));
        final Uri uri = Uri.parse('http://localhost/person');
        final bindHandler = sb.bind(singleArgMockHandler());
        final routeHandler = (sr.router()..post('/person', bindHandler))
            .handler;

        final request = _createRequest(uri);
        final responseFuture = syncFuture(() => routeHandler(request));
        responseFuture.then((_) {
          mockHandler1.calls('call', personWithName(equals('fred')))
              .verify(happenedOnce);
        });
        expect(responseFuture, completes);
      });


    });

    group('should set response headers when annotation present', () {

      s.Request _createRequest(Uri uri) {
        final json = JSON.encode(new ConstructorPerson('fred').toJson());
        final body = new Future.sync(() => json.codeUnits).asStream();
        return new s.Request('POST', uri, body: body);
      }


      _PersonHandler1 singleArgMockHandler() => tweakedResponseHandler;

      setUp(() {
        mockHandler1.when(callsTo('call'))
            .alwaysReturn(new ConstructorPerson('barney'));
      });


      test('', () {
        final Uri uri = Uri.parse('http://localhost/person');
        final bindHandler = sb.bind(tweakedResponseHandler);
        final routeHandler = (sr.router()..post('/person', bindHandler))
            .handler;

        final request = _createRequest(uri);
        // final responseFuture = routeHandler(request);
        final responseFuture = new Future.sync(() => routeHandler(request));

        responseFuture.then((r) {
          mockHandler1.calls('call').verify(happenedOnce);
          expect(r, new isInstanceOf<s.Response>());
          s.Response response = r;
          expect(response.headers['content-type'], equals('application/json'));
          response.readAsString().then((str) {
            expect(JSON.decode(str)['name'], equals('barney'));
          });

          // should be what we set in annotation
          expect(response.statusCode, equals(201));
          expect(response.headers['location'],
            equals('http://localhost/person/barney'));

        });
        expect(responseFuture, completes);
      });
    });

    group('should inject custom objects', () {

      request() {
        final req = new s.Request('GET', Uri.parse('http://localhost/person'));
        return sp.addPathParameters(req, {'name' : 'fred'});
      }

      final customObjects = {
        PersonLookupClient: (req) => new Future.value(new PersonLookupClient())
      };

      final handler = sb.bind((String name, PersonLookupClient client) =>
          client.lookup(name),
          customObjects: customObjects);

      _response() => handler(request());

      test('', () {
        expect(_response(), completes);
      });

      test('and handle a factory that returns a future', () {
        return _response().then((s.Response resp) => resp.readAsString())
            .then((bodyStr) {
              expect(bodyStr, equals('{"name":"fred"}'));
        });
      });

    });

    group('should validate parameters', () {
      request(bool withAge) {
        final req = new s.Request('GET', Uri.parse('http://localhost/person'));
        final req2 = sp.addPathParameters(req, {'name' : 'fred'});
        return withAge ? sp.addPathParameters(req2, {'age' : 10}) : req2;
      }

      final handler = sb.bind((ConstrainedPerson person) => "Hello",
          validateParameters: true);

      _response(bool withAge) => handler(request(withAge));

      group('and when constraint violated', () {
        test('throws a BadRequestException', () {
          expect(_response(false), throwsA(new isInstanceOf<BadRequestException>()));
        });

        test('throws a BadRequestException with errors populated', () {
          expect(_response(false), throwsA((
              (BadRequestException e) => e.toMap()['errors'] != null)));
        });
      });


      test('and completes normally when no constraint violated', () {
          expect(_response(true), completes);
      });

    });

    group('should validate return', () {
      final request = new s.Request('GET', Uri.parse('http://localhost/person'));

      handler(int age) => sb.bind(() => new ConstrainedPerson.build(
          name: 'fred', age: age),
          validateReturn: true);

      _response([int age]) => handler(age)(request);

      group('and when constraint violated', () {
        test('throws a HttpException', () {
          expect(_response(), throwsA(new isInstanceOf<HttpException>()));
        });

        test('throws a HttpException with status 500', () {
          expect(_response(), throwsA((
              (HttpException e) => e.status == 500)));
        });

        test('throws a HttpException with errors populated', () {
          expect(_response(), throwsA((
              (HttpException e) => e.toMap()['errors'] != null)));
        });
      });


      test('and completes normally when no constraint violated', () {
          expect(_response(10), completes);
      });

    });
  });

}


s.Response _handlePerson(AbstractPerson person, s.Request request) {
//  print(person);
  return new s.Response.ok('yippee - ${person.toJson()}');
}

Matcher personWithName(matcher) => _personMatcher("name", matcher,
    (AbstractPerson p) => p.name);

Matcher _personMatcher(String fieldName, matcher, Getter getter)
    => fieldMatcher("Person", fieldName, matcher, getter);

Matcher fieldMatcher(String className, String fieldName, matcher,
                     Getter getter) =>
    new FieldMatcher(className, fieldName, matcher, getter);


typedef Getter(object);

class FieldMatcher extends CustomMatcher {
  final Getter getter;

  FieldMatcher(String className, String fieldName, matcher, this.getter)
      : super("$className with $fieldName that", fieldName, matcher);

  featureValueOf(actual) => getter(actual);
}


abstract class AbstractPerson {
  String get name;

  Map toJson() => { 'name': name };

  String toString() => '$runtimeType[name: $name]';
}

class ConstructorPerson extends AbstractPerson {
  final String name;

  ConstructorPerson(this.name);

  ConstructorPerson.build({String name}) : this(name);

  ConstructorPerson.fromJson(Map json) : this(json['name']);
}

class FieldPerson extends AbstractPerson {
  String name;

  FieldPerson();

  FieldPerson.fromJson(Map json) : this.name = json['name'];
}

class SetterPerson extends AbstractPerson {
  String _name;

  void set name(String name) {
    _name = name;
  }

  String get name => _name;

  SetterPerson();

  SetterPerson.fromJson(Map json) { this.name = json['name']; }
}

class ConstrainedPerson extends ConstructorPerson {
  @NotNull()
  final int age;

  ConstrainedPerson.build({String name, this.age}) : super.build(name: name);

}

class PersonLookupClient {
  Future<ConstructorPerson> lookup(String name) =>
      new Future.value(new ConstructorPerson.build(name: name));
}