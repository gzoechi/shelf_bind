// Copyright (c) 2014, The Shelf Bind project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_bind.convert.test;

import 'package:shelf_bind/src/converters.dart';
import 'package:unittest/unittest.dart';

main() {
  final String testDateStr = '2014-07-14 08:38:00.209';
  final DateTime testDate = DateTime.parse(testDateStr);
  final String testUriStr = 'http:foo:7899/blah';
  final Uri testUri = Uri.parse(testUriStr);

  group('convertTo', () {
    test('converts a string to a int', () {
      expect(convertTo(num, "10"), equals(10));
      expect(convertTo(num, "10"), new isInstanceOf<int>());
    });

    test('converts a string to a double', () {
      expect(convertTo(num, "10.2"), equals(10.2));
      expect(convertTo(num, "10.2"), new isInstanceOf<double>());
    });

    test('converts a string to a bool', () {
      expect(convertTo(bool, "true"), equals(true));
      expect(convertTo(bool, "false"), equals(false));
      expect(convertTo(bool, "foobar"), equals(false));
      expect(convertTo(bool, null), equals(false));
    });

    test('converts a String to a DateTime ', () {
      expect(convertTo(DateTime, testDateStr), equals(testDate));
    });

    test('converts a String to a Uri ', () {
      expect(convertTo(Uri, testUriStr), equals(testUri));
    });

    test('throws FormatExcpetion on invalid num input', () {
      expect(() => convertTo(num, "fred"),
          throwsA(new isInstanceOf<FormatException>()));
    });
  });

  group('convertFrom', () {
    test('converts a int to a String', () {
      expect(convertFrom(num, 10), equals("10"));
    });

    test('converts a double to a String', () {
      expect(convertFrom(num, 10.2), equals("10.2"));
    });

    test('converts a bool to a String', () {
      expect(convertFrom(bool, true), equals("true"));
      expect(convertFrom(bool, false), equals("false"));
    });

    test('converts a DateTime to a String', () {
      expect(convertFrom(DateTime, testDate), equals(testDateStr));
    });
  });

}